\section{Solutions}

Several solutions have been proposed to countermeasure the attacks described 
above, including digital signatures, encryption and different methods to rate 
the thrustworthiness of nodes.

The authors of AODV and OLSR do not specify any measures to provide security, 
but some vulnerabilities of the respective protocol and suggestions to make 
them more secure are described in both RFCs~\cite{rfc-olsr, rfc-aodv}.

This section will give an overview on how to prevent intrusions using different 
techniques of encryption and authentication, how to detect an intrusion and 
how to respond to maintain the network's functionality.

\subsection{Intrusion Prevention}

As many attacks are based on impersonation or the distribution of false 
information, these are two important things to prevent. To prevent 
impersonation or at least detect it, authenticity of a sender must be assured. 
This can be done by using digital signatures or public-key cryptography, 
respectively~\cite{survey-security-wmn}. Software like PGP provides a reliable 
implementation of such cryptographic techniques.

What makes this hard to apply in pratice is the distribution of the keys. A 
WMN's topology is not constant, but changes as nodes join and leave the network 
dynamically. To enable authenticated communication over multiple hops, every 
node needs to have the public keys at least of its neighbours. These keys could 
be shared offline before the actual mesh network is 
established~\cite{sec-architecture}, but that would 
make it impractical for others to join the network and be trusted. A different 
approach suggests the use of a trusted server, that holds keys or certificates, 
respectively~\cite{survey-security-wmn}. This has several drawbacks, too, 
because the server would have to 
be contactable for every node and its service would have to be available all 
time, which makes it a single point of failure. A central instance on that 
nodes depend, however, is contrary to the idea of mesh networks that are 
decentralized in their nature and still able to operate, and do not depend on a 
single instance \cite{survey-wmn}. To mend some of these disadvantages, not 
one, but several 
multiple servers could be used. The network could then be split up into 
disjoint groups such that every server is assigned to one group and holds the 
keys/certificates for the members of this group. This would reduce the load on 
a single machine, and such a service could be provided by trusted nodes.\\
To now prevent that data like the hopcount is compromised by an intermediate 
node, its integrity must be assured. In routing, this can be done by using 
one-way hash chains. A packet will only be forwarded $n$ times, with $n$ being 
defined by the protocol. A node that wants to send a control packet attaches 
a random string $s$ to it, and applies a one-way hash function $h$ to it $n$ 
times. This hash value must be publicly available (e. g. by a trusted server). 
The first node to receive the packet can then verify the sender by applying the 
same hash function $n$ times and comparing the result to the public value. 
Before forwarding the packet, it applies the same hash function to the string 
and replaces $s$ with $h(s)$. The next node to receive the packet can again 
verify it by applying $h$ to the string $h(s)$, but only $n - k$ times, where 
$k$ is the hop count. If the result doesn't match the public value, the 
received hop count or hash value must be forged~\cite{provable}.

Availability, however, can not be guaranteed, since nodes cannot be forced to 
forward packets. But in reasonably large WMNs, more than one path to a 
destination can often be found. These additional routes can then be used to 
receive acknowledgements from the destination, or to send a packet numerous 
times, with one route maybe avoiding a malicious node \cite{blackhole-adhoc}.

\subsection{Intrusion Detection}

The disruption of a WMN's functionality is usually caused by malicious nodes 
dropping packets. Therefore the first step is to detect that packets do not 
arrive at their destination, followed by detecting the malicious node(s).\\
A simple approach to check if a packet arrives is to require an acknowledgement 
(\texttt{ACK}) by the receiver. Since such an \texttt{ACK} is just a simple packet, too, its 
arrival cannot be guaranteed either. To circumvent this, a different route than 
the one the original packet took can be used. These routes of course have to be 
discovered first, which generates additional overhead to the \texttt{ACK} packets that 
have to be delivered \cite{blackhole-adhoc}. Also, as long as the malicious 
node itself is not 
identified, it can still be part of alternative routes, and even fake \texttt{ACKs}. 
Thus, the \texttt{ACKs} have to be signed to prove their authenticity, which in turn 
requires public-key infrastructure \cite{defending}.

Another way to detect malicious nodes is the use of watchdogs. Wireless signals 
are propageted omni-directional, which allows nodes to listen to traffic that 
is not destined for them. These nodes could then act as watchdogs that check if 
nodes around them forward packets correctly and warn other nodes if this is not 
the case. The range of the wireless signals limits the area in which the 
monitoring is possible, so several watchdogs would have to be applied. 
Additional or special hardware may be used for this, as eavesdropping on the 
traffic consumes much more energy than normal operation. Attackers could also 
use directional antennas to bypass the watchdogs \cite{defending}.

To improve the effectiveness of watchdogs, they could be applied as mobile 
software agents instead of being deployed on a single node, and thus change 
their location to expand the area being checked and make it harder to be 
detected.

Instead of just listening to network traffic, a node could also send \texttt{RREQ} to 
destinations it already has a route for. A malicious is expected to answer to 
every \texttt{RREQ} immediatley, claiming it has a reliable route to the destination. A 
comparison to the known route can then detect a fake route easily~\cite{honey}.

For the detection of wormhole attack several methods have been developed. Some 
approaches make use of signed accurate timestamps, but this methos is not 
suitable for attacks that involve multiple nodes, since they could share their 
keys. The final sender could then sign the packet with a legitimate senders key 
and would not be detected. Maheshwari et al. therefore propose a detection 
system that investigates the connectivity graph and looks for forbidden 
substructures, that should not occur in legal connectivity graphs 
\cite{wormhole}.


\subsection{Intrusion Response}

After a attack has been detected, all other nodes should be warned and as soon 
as possible stop using the malicious node(s) (if identified) as relay points.
The broken routes must be deleted from the routing tables and new routes have 
to be established, because the network now might be partitioned. While the 
latter is done easily, since discovering routes is an essential part of every 
protocol, at least a special message type is needed to accomplish the former. 
Also, an effecient flooding mechanism must be provided, and warning messages 
must be signed by a trusted entity.
