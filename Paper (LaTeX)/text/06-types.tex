\section{Different Attack Types}

Attacks on the network layer can generally be divided into two groups, attacks 
on the control plane and on the data plane. While the former try to disrupt the 
network's routing functionality, e.g. by creating falsified control packets or
injecting invalid route-error messages, the latter disturb the ability to 
securely forward data, e.g. by spying on or dropping 
packets~\cite{survey-security-wmn, sec-routing}.

Referring to these definitions, not all kinds of attacks fit perfectly into one 
group or the other, so intersections may occur.

Since this paper discusses security in routing, most of the described attacks 
will take place on the control-plane.

\subsection{Black-hole Attack}

With this type of attack, a malicious node drops every packet it receives. 
This 
disrupts the network's functionality by disabling communication between all 
nodes that choose the malicious one as a relay point.

To improve the efficiency of this attack, the attacker tries to attract and 
subsequently drop as many packets as possible. This is usually done by 
advertising (non)exisiting 
routes as low-cost and/or by impersonating other participants of the network 
to capture packets intended for them~\cite{survey-security-wmn}.

Due to the use of these methods, black-hole attacks are usually considered 
control-plane attacks. A simpler version of this attack, where a node 
doesn't behave odd except by dropping all data packets, would fit the 
definition of data-plane attack.

\subsection{Grey-hole Attack}

This attack works similar to the black-hole attack in that packets are dropped 
to disrupt communication. The difference is that not all, but only select
packets are dropped~\cite{survey-security-wmn}.

This prevents the malicious node from being easily detected as an attacker, 
since the packet loss rate looks like it could be caused by transmission 
problems on lower layers.

The malicious node possibly also just wants to stop two or more specific 
participants from communicating instead of disrupting the whole network.

\subsection{Worm-hole Attack}

To perform a worm-hole attack, two or more distant nodes (referred to as Node A 
and Node B) are needed, that are  interconnected by a medium faster than the 
one used in the mesh network (e.g. an ethernet cable). The packets received by 
node A are sent to node B via the fast medium, then get replayed by B and vice 
versa.

This has the effect that other nodes assume neighbours of A to be neighbours of 
B (and vice versa), which affects routing in that other nodes now might 
route packets via A or B instead of using valid routes established before.

A and B can then drop packets and/or spy on the communication on the other end 
of the worm-hole link~\cite{survey-security-wmn}.

The manipulation of the routing makes this a control-plane attack, too.

\begin{figure}
        \centering
        \includegraphics[width=0.45\textwidth]{figures/wormhole.png}
        \caption{
        Wormhole Attack - A and B are malicious nodes, the thick line 
        represents the wormhole link
        }
\end{figure}

\subsection{Sybil Attack}

In a sybil attack~\cite{survey-security-wmn}, a node creates several identities 
in the network, each presenting itself and acting as a normal participant. 
Other nodes, which assume these fake nodes to be legitimate, may then add them 
to their routing paths and forward data to them. All this data passes through 
the same physical node, which not only decreases performance, but also allows 
the malicious node to perform any other attack like dropping, replaying etc. 

This attack also fits the definition of a control-plane attack, as the 
announcement of an identity is communicated over control packets.

\subsection{Route Error Injection}

An attacker creates forged route-error messages and injects them into the 
network. Legitimate nodes will then remove the links advertised as broken from 
their routing tables, which forces them to use nonoptimal routes or to request 
new ones~\cite{survey-security-wmn}. All of this takes place on the 
control-plane.

\subsection{Sleep Deprivation}

A sleep deprivation attack is similar to a typical Denial-of-Service attack. An 
attacker keeps other nodes busy by constantly sending them route requests or 
other unnecessary packets, that might require calculations and therefore CPU 
ressources when received. This makes a sleep deprivation attack especially 
dangerous for battery operated devices~\cite{survey-security-wmn}.

Control packets that initiate further actions are most often used since they 
are more effective, which makes this attack a control-plane attack, too.

