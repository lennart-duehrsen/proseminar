\section{Protocol Specific Implementation of Selected Attacks}

This section outlines how the described methods and attacks could be 
implemented and deployed in networks that use either OLSR or AODV as their 
routing protocol. Deeper knowledge of those protocols will be imparted as 
needed.

\subsection{OLSR}

Since most attacks use forged packets for their purposes, the general format of 
OLSR packets is examined first. Every message that contains information 
concerning the routing and not user data follows the same structure, as 
described by section 3.3 in RFC 3626~\cite{rfc-olsr}.

In the following, a message specifies a logical unit like a \texttt{HELLO} message or a 
\texttt{TC} message, whereas a packet denotes a network packet, e.g. a UDP packet.
Thus, one packet can contain several messages. The content of such a packet 
refers to the payload, omitting things such as IP and UDP headers.

Every OLSR message carries the originator's address. The source address of the 
IP header would not be sufficient for this, as it changes with every 
forwarding. The fact that this address (like other information) is carried in 
the payload is the first point that harms the integrity and authenticity of 
a message, because such information can easily be changed before forwarding the 
packet.

Besides the originator's address, every message contains its type (\texttt{HELLO}, \texttt{TC}, 
etc.), its size (in bytes), its time to live and the hop count, and the 
validity time (vtime). The vtime indicates for how long a after reception a 
node considers the information in the message as valid. All of these can be 
adjusted just as the originator's address.
{\ }\\

\subsubsection{Black-hole Attack}

To perfom an effective black-hole attack, as many packets as possible should be 
attracted. Advertising (fake) links as low cost has been mentioned as one 
method, which means having to manipulate the link quality. To measure the link 
quality, a metric must be used, but the RFC~\cite{rfc-olsr} does not define 
which should be used. In fact, it only suggest distributing information about 
the link quality with the local link set, but does not require it. One commonly 
used metric however is ETX, the Expected Transmission Count. It is based on the 
number of received \texttt{HELLO} messages compared to the interval in which they are 
sent, so to distort the results, a malicious node could send \texttt{HELLO} messages in 
a shorter interval than announced.

Another way to attract packets is acting as an MPR for as many nodes as 
possible. A node is selected as an MPR by others, if it has routes to the 
selector's two hop neighbours. If this is the case is checked by looking into 
the \texttt{TC} messages sent by the (malicious) node, but those can be falsified easily.
{\ }\\

\subsubsection{Worm-hole Attack}

Performing a worm-hole attack is simple, disregarding the fast link between the 
malicious nodes. Only the originator address has to be adjusted before 
replaying the messages.
{\ }\\

\subsubsection{Sybil attack}

For a sybil attack, fake identities can be created or other nodes can be 
impersonated. Creating a fake identity is simply done by sending 
out forged messages with the desired originator address and a lists of links 
the malicious node allegedly has.

When impersonating existing nodes, it is possible that a malicious node cannot 
completely replace an existing node. That is, the impersonated node is still 
active and other participants may use its hardware address instead of the 
attacker's. To countervail this, an attacker should prevent the distribution of 
the impersonated node's packets, e. g. by becoming its MPR.
{\ }\\

\subsubsection{Sleep Deprivation}

Since this attack targets a nodes ressources, it is desirable to make the 
attacked nodes consume as much power and computing time as possible. The first 
step to accomplish this would be to send as many packets as possible to the 
victim node. These packets can contain control packets as well as data packets 
that are to be forwarded. To enlarge the impact of the attack to multiple 
nodes, the TTL value should be set to the highest value possible (usually 255) 
and the vtime to the smallest value possible. A malicious node could also 
forward any packet it receives, regardless if it should be forwarded according
to section 3.4.1 in RFC 3626 \cite{rfc-olsr}, and additionally not increase the 
packet's hop count.

\subsection{AODV}

\subsubsection{Black-hole Attack}

A simple version of this attack, where an attacker only refrains from 
forwarding packets, is done easily. However, nodes monitor the link status 
\cite{rfc-aodv} and are likely to detect a broken link soon, then sending 
route-error messages (\texttt{RERRs}) which would stop nodes from sending packets to the 
attacker. Thus, to 
make this attack effective, links must be reestablished the whole time and/or 
multiple identities have to be used.
{\ }\\

\subsubsection{Worm-hole Attack}

As in OLSR, this attack is implemented easily, as long as a sufficient link 
between the nodes performing the attack exists. When using AODV, the sequence 
number of the packets to replay should be incremented to make sure other nodes 
treat the information received as the newest available.
{\ }\\

\subsubsection{Sybil Attack}

In contrast to OLSR, there is no such thing as \texttt{HELLO} messages. Any route is 
established on demand, so nodes only get to know about the existence of another 
when a route is needed or broken. Thus, multiple impersonation of nodes, as 
done in a typical sybil attack takes place in the mechanism of route 
establishing. Sending packets with different originator addresses, but same 
content would not be very effective, since no additional packets would be 
attracted. Instead, an attacker should act normal until parts of the networks 
topology are known, then sending \texttt{RERRs} regarding routes of other nodes in their 
names (using their IP addresses as the respective originator address). The 
attacker can then establish routes using fake originator addresses (for 
instance the same routes that existed before), which leads to other nodes 
sending their packets to the attackers hardware address.
{\ }\\

\subsubsection{Route-Error Injection}

By injecting forged \texttt{RERRs} into the network, an attacker forces other nodes to 
remove the mentioned links from their routing tables. Said nodes then have to 
establish new routes their desired destinations, or might use non-optimal 
routes. This causes dysfunction or at least a delay. Nodes will also spread 
this wrong information, and therefore this attack has a high impact on large 
parts of the network.
{\ }\\

\subsubsection{Sleep Deprivation}

Similar to the OLSR implementation of this attack, the focus lies on the 
actions a packet initiates when received by a victim. Thus, the three types of 
control packets used by AODV are examined.

When a node receives a \texttt{RREP}, regardless if it is the destination node, it 
checks the content for its currentness (by looking at the sequence number), 
possibly updates its routing table and maybe forwards the packets.

The use of ressources is similar to receiving a \texttt{RERR}. Again, the currentness is 
checked, the routing table will be updated and the packet forwarded.

A received \texttt{RREQ} only requires notable effort, if there's no route to the 
destination in the recipients routing table. Thus, it can be seen that limiting 
the attack to a single control packet would not be extremly effective. A 
combination of these, however, could cause more damage. By sending a \texttt{RERR} which 
denotes a specific route as broken and a \texttt{RREQ} shortly afterwards, a node can be 
kept busy. This can and should be done for other routes, too, to make this 
attack as effective as possible.

In a different version, an attacker could send lots of \texttt{RREP} with routes to 
non-existent nodes, which would cause a routing table overflow 
\cite{survey-security-wmn}.