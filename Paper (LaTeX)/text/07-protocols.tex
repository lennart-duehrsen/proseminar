\section{Routing Protocols}

Routing protocols for WMNs can be classified as either proactive, reactive, or 
hybrid. 
A proactive protocol, also called table-driven, maintains all available routes 
during the whole time of operation. Control messages to detect neighbours and 
exchange routing information are sent regularly.

A reactive protocol on the other hand begins looking for a route only after it 
is needed. The initiating node sends a route request to its neighbours 
containing the recipients address, who will then forward it in turn or, if the 
recipient is in the set of their neighbours, send back a route reply to the 
initiator~\cite{survey-threats}.

Both approaches have several drawbacks. Proactive protocols often don't scale 
in large networks, because too many possible routes must be maintained. 
Reactive protocols usually do not face this problem, since routes are 
established on demand, which on the other hand implicates a delay before actual 
data can be send.

It shall be mentioned that hybrid protocols exists, which try to combine the 
advantages of both approaches while avoiding the drawbacks~\cite{hybrid}.

In the following, brief introductions to one proactive and one 
reactive protocol are given, namely the Optimized Link State Routing Protocol 
(OLSR)~\cite{rfc-olsr} and the Ad-hoc On Demand Distance Vector Routing 
Protocol (AODV)~\cite{rfc-aodv}.

\subsection{Optimized Link State Routing}

OLSR~\cite{rfc-olsr} is a proactive routing protocol for wireless mesh 
networks, and works as follows: Nodes broadcast \texttt{HELLO} messages at a regular 
interval, which are not forwarded by receivers. Its purpose is for other nodes 
to detect the sender as a neighbour. If \texttt{HELLO} messages of two nodes are 
received by each other, a symmetric link exists.

\texttt{HELLO} messages furthermore contain the interval in which they are sent and a 
list of neighbours already known to the sender. Said interval and the number of 
actually received packets are then used to determine the link quality.

To now inform the whole network about links to neighbours, topology control (\texttt{TC})
messages are used. They contain the advertised neighbour set, which consists of 
the addresses of all neighbours to which a symmetric link exists. The \texttt{TC} 
messages are then flooded through the whole network. Every \texttt{TC} message also has 
a sequence number, by which other nodes can tell which information is newer and 
thus more accurate: Everytime a node recognizes a change in its advertised 
neighbour set, it sends a new \texttt{TC} message with a sequence number higher than 
that of the last \texttt{TC} message sent, so a higher sequence number denominates newer 
information.

To make the flooding more efficient, OLSR introduces the concept of multipoint 
relays (MPRs). Each node selects from its neighbours those that have a 
symmetric link to it and routes to all its 2-hop neighbours, i.e. the nodes 
heard by a neighbour, as an MPR. A new \texttt{TC} message is then broadcasted, but only 
forwarded by MPRs.

\subsection{Ad-hoc on Demand Distance Vector Routing}

As a reactive protocol, AODV~\cite{rfc-aodv} does not play any role as long as 
valid routes to 
desired destinations exists. Otherwise, an originator node starts the route 
discovery process by broadcasting a route request (\texttt{RREQ}) to the network. It 
contains its own address, the destination's address and an ID identifying the 
particular \texttt{RREQ}. Furthermore, two sequence numbers are sent with every \texttt{RREQ}, 
the originator sequence number (OrigSeqNum) and the destination sequence number 
(DestSeqNum). A node's own sequence number is incremented with every AODV 
message sent, so other nodes know which information is newer. Accordingly, the 
destination sequence number denotes the latest information about the 
destination that the originator is aware of.

A node that receives a \texttt{RREQ} has now two options. If it is the destination 
or has a valid route to it with a sequence number greater than the DestSeqNum 
from the \texttt{RREQ}, it unicasts a route reply (\texttt{RREP}) to the originator, using the 
reverse route in which the \texttt{RREQ} arrives. This is possible, because every node 
that can not answer with a \texttt{RREP} forwards the \texttt{RREQ} after adding its own address 
to the list of nodes that forwarded this message before, which would be the 
second option. This way, every node is able to trace back a route to the 
originator of any \texttt{RREQ}.

Regardless of the option chosen, every receiver of a \texttt{RREQ} adds the reverse 
route outlined above to its routing table (if the OrigSeqNum in the \texttt{RREQ} is 
greater than the ones received with AODV messages before).
